// imports
const request = require('request');
const JSONParser = require('../helperFunctions/parseJson');
const APIID = '2ed0db15bd0055350df89ec320fb5f3b';

// find weather in a particular city
// note on callback: in the web service, the callback's purpose will be to
// send the weather report back to the chatbot, once it's recieved.
exports.getWeatherInCity = function (city, callback) {
    let url = ('https://api.openweathermap.org/data/2.5/weather?'
        + 'q=' + city
        + '&APPID=' + APIID);

    let weatherReport = 'Sorry, no weather report is available for this city.';

    // make request to open weather api
    if (city !== undefined) {
        request(url, { json: true },
            (err, responseFromWeatherAPI, body) => {

                if (err) {
                    return callback(err);
                }

                weatherReport = '';

                let description = 'no description';
                let kelvins = 'no temperature';
                let humidity = 'no humidity';
                let windSpeed = 'no wind';

                let weather = responseFromWeatherAPI.body['weather'];
                JSONParser.getProperty(weather, 'description', (parseResult) => { description = parseResult; });

                let main = responseFromWeatherAPI.body['main'];
                JSONParser.getProperty(main, 'temp', (parseResult) => { kelvins = parseResult; });
                JSONParser.getProperty(main, 'humidity', (parseResult) => { humidity = parseResult; });

                let wind = responseFromWeatherAPI.body['wind'];
                JSONParser.getProperty(wind, 'speed', (parseResult) => { windSpeed = parseResult; });
                JSONParser.getProperty(wind, 'deg', (parseResult) => { windDir = parseResult; });

                let fahrenheit = Number(kelvins * (9.0 / 5.0) - 459.67).toFixed(2);
                let celsius = Number(kelvins - 273.15).toFixed(2);

                weatherReport = 'Conditions in ' + city + ': ' + description +
                    '\n.The temperature is ' + fahrenheit + ` degrees fahrenheit, and ` + celsius + ' degrees celsius.' +
                    '\nHumidity is ' + humidity + `. Wind speed is ` + windSpeed +  '.' +
                    '\nAnything else I can help you with?';

                return callback(weatherReport);
            });
    }
    return weatherReport;
}