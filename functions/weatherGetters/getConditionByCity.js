// imports
const request = require('request');
const JSONParser = require('../helperFunctions/parseJson');
const APIID = '2ed0db15bd0055350df89ec320fb5f3b';

// "condition" here means a specific weather descriptor, such as "rainy", "sunny", etc.
exports.getConditionByCity = function (city, condition, callback) {
    let url = ('https://api.openweathermap.org/data/2.5/weather?'
        + 'q=' + city
        + '&APPID=' + APIID);

    let weatherReport = 'Sorry, no weather report is available for ' + city;

    // make request to open weather api
    if (city !== undefined) {
        request(url, { json: true },
            (err, responseFromWeatherAPI, body) => {

                if (err) {
                    return callback(err);
                }

                weatherReport = '';

                let description = 'no description';

                let weather = responseFromWeatherAPI.body['weather'];
                JSONParser.getProperty(weather, 'description', (parseResult) => { description = parseResult; });

                if(description.includes(condition)){
                    weatherReport = 'Yes. Conditions in ' + city + ' are: ' + description;
                } else {
                    weatherReport = 'No. Conditions in ' + city + ' are: ' + description;
                }

                return callback(weatherReport);
            });
    }
    return weatherReport;
}