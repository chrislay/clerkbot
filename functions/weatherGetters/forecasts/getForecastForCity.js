// imports
const request = require('request');
const JSONParser = require('../../helperFunctions/parseJson');
const APIID = '2ed0db15bd0055350df89ec320fb5f3b';

// find weather in a particular city
// note on callback: in the web service, the callback's purpose will be to
// send the weather report back to the chatbot, once it's recieved.
exports.getForecastForCity = function (city, datetime, callback) {
    let targetDate = datetime.substring(0, 11);
    let targetTime = datetime.substring(11, 19);

    let url = ('https://api.openweathermap.org/data/2.5/forecast?'
        + 'q=' + city
        + '&APPID=' + APIID);

    let weatherReport = 'Sorry, no weather report is available for this city.';

    // make request to open weather api
    if (city !== undefined) {
        request(url, { json: true },
            (err, responseFromWeatherAPI, body) => {

                weatherReport = '';
                let count = 0;
                let startAddingToWeatherReport = false;

                if (err) {
                    return callback(err);
                }

                for (index in responseFromWeatherAPI.body['list']) {
                    let elementDate = responseFromWeatherAPI.body['list'][index]['dt_txt'].substring(0, 11);
                    let elementTime = responseFromWeatherAPI.body['list'][index]['dt_txt'].substring(11, 19);


                    if (startAddingToWeatherReport === false && elementDate.includes(targetDate) && elementTime.includes(targetTime)) {
                        startAddingToWeatherReport = true;
                    }

                    if (startAddingToWeatherReport) {
                        JSONParser.getProperty(responseFromWeatherAPI.body['list'][index], 'weather', (parseResult) => { weather = parseResult; })

                        weatherReport += `Here's the forecast for ` + targetDate + ':\n';
                        weatherReport += elementTime + '\n';

                        let description = 'N/A';
                        JSONParser.getProperty(responseFromWeatherAPI.body['list'][index]['weather'], 'description', (parseResult) => { description = parseResult; });
                        weatherReport += description + '\n';

                        let main = {};
                        JSONParser.getProperty(responseFromWeatherAPI.body['list'][index], 'main', (parseResult) => { main = parseResult; });

                        let kelvins = 0;
                        JSONParser.getProperty(main, 'temp', (parseResult) => { kelvins = parseResult; });

                        // let fahrenheit = Number(kelvins * (9.0 / 5.0) - 459.67).toFixed(2);
                        let celsius = Number(kelvins - 273.15).toFixed(2);
                        weatherReport += celsius + ' deg C';

                        weatherReport += '\n\n';

                    }
                }

                return callback(weatherReport);
            });
    }
}