// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
const functions = require('firebase-functions');
const JSONParser = require('./helperFunctions/parseJson');

const findWeatherByCityModule = require('./weatherGetters/getWeatherInCity');
const findWeatherByZipModule = require('./weatherGetters/getWeatherAtZip');
const getForecastForCityModule = require('./weatherGetters/forecasts/getForecastForCity');
const getForecastForZipModule = require('./weatherGetters/forecasts/getForecastForZip');
// const findConditionByCityModule = require('./weatherGetters/getConditionByCity');
// const findConditionByZipModule = require('./weatherGetters/getConditionByZip');

exports.processRequest = functions.https.onRequest(
    (request, response) => {
        // todo: determine intent
        let intent = request.body['queryResult']['intent']['displayName'];

        // response.send({
        //     "fulfillmentText": intent
        // });

        if (intent.includes('get weather in city')) {
            // figure out which city they want

            let city = 'no city';
            JSONParser.getProperty(request.body, 'geo-city', (parseResult) => { city = parseResult; });

            findWeatherByCityModule.getWeatherInCity(city, (weatherReport) => {
                response.setHeader('Content-Type', 'application/json');

                response.send({
                    "fulfillmentText": weatherReport
                });
                return weatherReport;
            });
        } else if (intent.includes('get weather at zip')) {
            let zip = '';
            JSONParser.getProperty(request.body, 'zip-code', (parseResult) => { zip = parseResult; });

            findWeatherByZipModule.getWeatherAtZip(zip, (weatherReport) => {
                response.setHeader('Content-Type', 'application/json');

                response.send({
                    "fulfillmentText": weatherReport
                });
                return weatherReport;
            });
        } 
        /*else if (intent.includes('is it rainy, sunny, etc? (with location)')) {
            let weatherDescriptor = '';
            JSONParser.getProperty(request.body, weather-descriptors, (parseResult) => { weatherDescriptor = parseResult; })

            let zip = '';
            JSONParser.getProperty(request.body, 'zip-code', (parseResult) => { zip = parseResult; });

            // use the zip code if we have it, because it's more precise. if we don't have it, go by city
            if (zip !== '') {
                findConditionByZipModule.getConditionAtZip(zip, weatherDescriptor, (weatherReport) => {
                    // send response
                    response.setHeader('Content-Type', 'application/json');

                    // build response
                    response.send({
                        "fulfillmentText": weatherReport
                    });
                    return weatherReport;
                });
            } else {
                let city = 'no city';
                JSONParser.getProperty(request.body, 'geo-city', (parseResult) => { city = parseResult; });

                findConditionByCityModule.getConditionByCity(city, weatherDescriptor, (weatherReport) => {
                    // send response
                    response.setHeader('Content-Type', 'application/json');

                    // build response
                    response.send({
                        "fulfillmentText": weatherReport
                    });
                    return weatherReport;
                });
            }
        }*/ 
        else if(intent.includes('get forecast (with location)') || intent.includes('is it rainy, sunny, etc? (with location)')) {
            let zip = '';
            JSONParser.getProperty(request.body, 'zip-code', (parseResult) => { zip = parseResult; });

            let city = '';
            JSONParser.getProperty(request.body, 'geo-city', (parseResult) => { city = parseResult; });

            let dateTime = '';
            JSONParser.getProperty(request.body, 'time', (parseResult) => { dateTime = parseResult; });

            // if datetime was recieved as "date"
            if(dateTime !== '') {
                JSONParser.getProperty(request.body, 'date', (parseResult) => { dateTime = parseResult; });
            }

            if(zip !== '') {
                getForecastForZipModule.getForecastForZip(zip, dateTime, (forecast) => {
                    response.setHeader('Content-Type', 'application/json');

                    response.send({
                        "fulfillmentText": forecast
                    });
                    return forecast;
                });
            } else if(city !== '') {
                getForecastForCityModule.getForecastForCity(city, dateTime, (forecast) => {
                    response.setHeader('Content-Type', 'application/json');

                    response.send({
                        "fulfillmentText": forecast
                    });
                    return forecast;
                });
            }
        } else {
            response.send({
                "fulfillmentText": `Not sure what you're asking for, here. Try saying it another way?`
            });
        }
    }
);
