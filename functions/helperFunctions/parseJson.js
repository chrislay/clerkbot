// helper function for finding parts of a json string
exports.getProperty = function(obj, lookup, callback) {
    for (property in obj) {
        if (property === lookup) {
            return callback(obj[property]);
        } else if (obj[property] instanceof Object) {
            // do some recursion, if you can't find the property on this level
            return this.getProperty(obj[property], lookup, callback);
        }
    }

    return 'NOT FOUND';
}